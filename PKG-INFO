Metadata-Version: 2.1
Name: gracedb-sdk
Version: 0.1.6
Summary: REST API SDK for GraceDB
Home-page: https://git.ligo.org/emfollow/gracedb-sdk
Author: Leo Singer
Author-email: leo.singer@ligo.org
License: GPL-3+
Project-URL: Bug Tracker, https://git.ligo.org/emfollow/gracedb-sdk/issues
Project-URL: Documentation, https://gracedb-sdk.readthedocs.io/
Project-URL: Source Code, https://git.ligo.org/emfollow/gracedb-sdk
Description: # gracedb-sdk
        
        A modern, performant REST API client for GraceDB, based on [Requests](http://python-requests.org/). For documentation, see https://gracedb-sdk.readthedocs.io.
        
        ## Benchmarks
        
        Time in seconds to perform common tasks [lscsoft/ligo-gracedb](https://git.ligo.org/lscsoft/gracedb-client). Measured on a residential Internet connection on the US East Coast.
        
        ![benchmarks compared to ligo-gracedb](https://git.ligo.org/emfollow/gracedb-sdk/snippets/92/raw)
        
        ## Notes for software packaging
        
        Software packaging files exist for the following systems:
        
        - RPM: https://git.ligo.org/packaging/rhel/python-gracedb-sdk
        - Debian: https://git.ligo.org/packaging/debian/gracedb-sdk
        - Conda: https://github.com/conda-forge/gracedb-sdk-feedstock
        
Platform: UNKNOWN
Classifier: Development Status :: 4 - Beta
Classifier: Environment :: Console
Classifier: Intended Audience :: Science/Research
Classifier: License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
Classifier: Operating System :: POSIX
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.5
Classifier: Programming Language :: Python :: 3.6
Classifier: Programming Language :: Python :: 3.7
Classifier: Programming Language :: Python :: 3.8
Classifier: Topic :: Internet
Classifier: Topic :: Scientific/Engineering :: Astronomy
Classifier: Topic :: Scientific/Engineering :: Physics
Requires-Python: >=3.6
Description-Content-Type: text/markdown
